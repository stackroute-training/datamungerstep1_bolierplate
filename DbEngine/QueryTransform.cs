﻿using System;
using System.Text.RegularExpressions;

namespace DbEngine
{
    public class QueryTransform
    {

        /*
    * This method will split the query string based on space into an array of words
    * and display it on console.
    */
        public string[] GetSplitString(string query)
        {
			string[] str =  query.Split();
			for(int i = 0; i < str.Length; i++)
            {
				str[i] = str[i].ToLower();
            }
			return str;
        }

        /*
   * Extract the name of the file from the query. File name can be found after a
   * space after "from" clause. Note: CSV file can contain a field that contains
   * from as a part of the column name. For eg: from_date,from_hrs etc.
   * 
   * Please consider this while extracting the file name in this method.
   */
        public string GetFileName(string query)
        {
			string str = "";
			string str2 = "";
			for (int i = 0; i < query.Length; i++)
            {
				if(query[i] == '.')
                {
					for(int j = i-1; j>=0; j--)
                    {
						if(query[j] == ' ')
                        {
							break;
                        }
                        else
                        {
							str += query[j];
                        }
                    }
					
					for(int l = str.Length-1; l >= 0; l--)
                    {
						str2 += str[l];
                    }
					
					str2 += '.';
					for(int k = i+1; k < query.Length; k++)
                    {
						if (query[k] == ' ')
                        {
							break ;
                        }
                        else
                        {
							str2 += query[k];
                        }
                    }
                }
            }
			return str2;   
        }

        /*
	 * This method is used to extract the baseQuery from the query string. BaseQuery
	 * contains from the beginning of the query till the where clause.
	 * 
	 * Note: 1. The query might not contain where clause but contain order by or
	 * group by clause 2. The query might not contain where, order by or group by
	 * clause 3. The query might not contain where, but can contain both group by
	 * and order by clause.
	 */
        public string GetBaseQuery(string query)
        {
			string[] str = query.Split();
			string str2 = "";
			foreach(string str3 in str)
            {
				if(str3 == "where")
                {
					break;
                }
                else
                {
					str2 += str3 + " ";
                }
            }
			return str2.Trim();
        }


        //Step II

        /*
	     * This method will extract the fields to be selected from the query string. The
	     * query string can have multiple fields separated by comma. The extracted
	     * fields will be stored in a String array which is to be printed in console as
	     * well as to be returned by the method
	     * 
	     * Note: 1. The field name or value in the condition can contain keywords as a
	     * substring. For eg: from_city,job_order_no,group_no etc. 2. The field name can
	     * contain '*'
	     * 
	     */

        public string[] GetFieldsNames(string queryString)
        {
			string[] str = queryString.Split();
			string str2 = "";
			foreach(string s in str)
            {
				if(s == "select")
                {
					continue;
                }
				else if(s == "from")
                {
					break;
                }
                else
                {
					str2 += s;
                }
            }
			string[] str3 = str2.Split(',');
            return str3;
        }

        /*
	     * This method is used to extract the conditions part from the query string. The
	     * conditions part contains starting from where keyword till the next keyword,
	     * which is either group by or order by clause. In case of absence of both group
	     * by and order by clause, it will contain till the end of the query string.
	     * Note: 1. The field name or value in the condition can contain keywords as a
	     * substring. For eg: from_city,job_order_no,group_no etc. 2. The query might
	     * not contain where clause at all.
	     */
        public string GetConditionsPartQuery(string queryString)
        {
			string[] str = queryString.Split();
			string str2 = "";
			for(int i = 0; i < str.Length; i++)
            {
				if(str[i] != "where")
                {
					str2 += str[i] + " ";
                }
                else
                {
					if(str[i] == "where")
                    {
						str2 += str[i];
					}
                    else
                    {
						break;
					}
					break;
                }
            }
			queryString =  queryString.Replace(str2, "");
            return queryString.Trim();
        }

        /*
	     * This method will extract condition(s) from the query string. The query can
	     * contain one or multiple conditions. In case of multiple conditions, the
	     * conditions will be separated by AND/OR keywords. for eg: Input: select
	     * city,winner,player_match from ipl.csv where season > 2014 and city
	     * ='Bangalore'
	     * 
	     * This method will return a string array ["season > 2014","city ='Bangalore'"]
	     * and print the array
	     * 
	     * Note: 1. The field name or value in the condition can contain keywords as a
	     * substring. For eg: from_city,job_order_no,group_no etc. 2. The query might
	     * not contain where clause at all.
	     */
        public string[] GetConditions(string queryString)
        {
			string query = GetConditionsPartQuery(queryString);
			string[] str = query.Split(" and ");
			for(int i = 0; i < str.Length; i++)
            {
				str[i] = str[i].ToLower();
            }
            return str;
        }

        //Step III

        /*
	     * This method will extract logical operators(AND/OR) from the query string. The
	     * extracted logical operators will be stored in a String array which will be
	     * returned by the method and the same will be printed Note: ------- 1. AND/OR
	     * keyword will exist in the query only if where conditions exists and and it
	     * contains multiple conditions. 2. AND/OR can exist as a substring in the
	     * conditions as well. For eg: name='Alexander',color='Red' etc. Please consider
	     * these as well when extracting the logical operators.
	     * 
	     */

        public string[] GetLogicalOperators(string queryString)
        {
			string[] str = queryString.Split();
			string[] ans = new string[2];
			int j = 0;
			for(int i = 0;i < str.Length; i++)
            {
				if(str[i] == "and" || str[i] == "or")
                {
					ans[j] = str[i];
					j++;
                }
            }
            return ans;
        }

        /*
	     * This method extracts the order by fields from the query string. Note: 1. The
	     * query string can contain more than one order by fields. 2. The query string
	     * might not contain order by clause at all. 3. The field names, condition
	     * values might contain "order" as a substring. For eg: order_number,job_order
	     * Consider this while extracting the order by fields
	    */
        public string[] GetOrderByFields(string queryString)
        {
            if (queryString.Contains("order"))
            {
				int indx = queryString.IndexOf("by");
				string str = queryString.Substring(indx + 3);
				string[] str2 = str.Split();
				return str2;
			}
            else
            {
				return new string[] {"Order by Clause is not Present in the given Statement"};
            }
			
        }

        //Step IV

        /*
	     * This method extracts the group by fields from the query string. Note: ------
	     * 1. The query string can contain more than one group by fields. 2. The query
	     * string might not contain group by clause at all. 3. The field names,
	     * condition values might contain "group" as a substring. For eg: newsgroup_name
	     * Consider this while extracting the group by fields
	    */
        public string[] GetGroupByFields(string queryString)
        {
            if (queryString.Contains("group"))
            {
				int indx = queryString.IndexOf("by");
				string str = queryString.Substring(indx + 3);
				string[] str2 = str.Split();
				return str2;
			}
			else
            {
				return new string[] { "Order by Clause is not Present in the given Statement" };
			}
        }

        /*
	 * This method extracts the aggregate functions from the query string. Note: 1.
	 * aggregate functions will start with "sum"/"count"/"min"/"max"/"avg" followed
	 * by "(" 2. The field names might contain "sum"/"count"/"min"/"max"/"avg" as a
	 * substring. For eg: account_number,consumed_qty,nominee_name
	 * 
	 * Consider this while extracting the aggregate functions
	 */
        public string[] GetAggregateFunctions(string queryString)
        {
			string[] str = queryString.Split();
			string[] str2 = str[1].Split(',');
            return str2;
        }
    }
}
